import { useState } from 'react';
import { useInterval } from 'react-use';

// TODO: rename to useStationMetaFeed
export const useStationInfo = (streamStatsUrl: string) => {
  const [listenerCount, setListerCount] = useState(0);

  useInterval(() => {
    const oReq = new XMLHttpRequest();
    oReq.addEventListener('load', function () {
      console.log('response loaded', this.status, this.responseText);
      if (this.status >= 200 && this.status < 400) {
        const data = JSON.parse(this.responseText);
        setListerCount(parseInt(data?.listeners));
      }
    });
    oReq.open('GET', streamStatsUrl);
    oReq.send();
  }, 10_000);

  return { listenerCount };
};
