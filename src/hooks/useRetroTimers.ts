import { useState } from 'react';
import { useInterval } from 'react-use';

const padNumberFn = (n: number) => Number(n).toFixed(0).padStart(2, '0');

export const useRetroTimers = (initialWorldDate: Date) => {
  const startMilis = initialWorldDate.getTime();
  const [currentTime, setCurrentTime] = useState('--:--');
  const [currentRuntime, setCurrentRuntime] = useState('00:00:00');
  const [isOddSecond, setSecondParity] = useState(false);

  useInterval(() => {
    const date = new Date();
    const timeParts = [date.getHours(), date.getMinutes()];
    setCurrentTime(timeParts.map(padNumberFn).join(':'));

    const deltaMilis = Math.abs(date.getTime() - startMilis);
    const deltaSeconds = deltaMilis / 1000;
    const deltaMinutes = deltaSeconds / 60;
    const deltaHours = deltaMinutes / 60;
    const timestampParts = [
      deltaHours % 24,
      deltaMinutes % 60,
      deltaSeconds % 60,
    ];
    setCurrentRuntime(timestampParts.map(padNumberFn).join(':'));

    setSecondParity(Math.floor(deltaSeconds % 2) === 0);
  }, 1_000);

  return {
    currentTime,
    currentRuntime,
    isOddSecond,
  };
};
