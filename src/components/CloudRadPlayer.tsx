import { FC } from 'react';

export type CloudRadPlayerProps = {
  iframeUrl: string;
  playlistUrl: string;
};

export const CloudRadPlayer: FC<CloudRadPlayerProps> = ({
  iframeUrl,
  playlistUrl,
}) => {
  return (
    <div>
      <iframe
        title="Embedded Player"
        width={380}
        height={380}
        frameBorder={0}
        src={iframeUrl}
      />
      <br />
      <a style={{ fontSize: '0.6rem', color: 'white' }} href={playlistUrl}>
        Want to stream in your own player?
      </a>
    </div>
  );
};
