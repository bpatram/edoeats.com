import { FC } from 'react';
import { useRetroTimers } from '../hooks/useRetroTimers';

export type RetroBackgroundProps = {
  title: string;
  initialWorldDate: Date;
  listenerCount: number | string;
};

export const RetroBackground: FC<RetroBackgroundProps> = ({
  title,
  initialWorldDate,
  listenerCount,
}) => {
  const { currentTime, currentRuntime } = useRetroTimers(initialWorldDate);

  return (
    <div>
      <div>{title}</div>
      <div>{currentTime}</div>
      <div>SLP {currentRuntime}</div>
      <div>{listenerCount}</div>
    </div>
  );
};
